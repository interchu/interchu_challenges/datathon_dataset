import numpy as np

from classifier_featuring import create_person_bag_of_items

VERBOSE = True


def test_create_person_bag_of_items():
    vocabulary = {0: 'DIAG1', 1: 'DIAG2', 2: 'DIAG3'}
    person_items = np.array(['DIAG1', 'DIAG2', 'DIAG1', 'DIAG1'])
    expected_boi = np.array([3, 1, 0])

    bag_of_item = create_person_bag_of_items(person_items, vocabulary)
    if VERBOSE:
        print('expected: {}'.format(expected_boi))
        print('computed: {}'.format(bag_of_item))
    assert expected_boi == bag_of_item
