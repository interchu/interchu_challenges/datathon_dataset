import pandas as pd

# Path conf
input_folder = '~/git/datathon_dataset/'
output_folder = '~/git/datathon_dataset/'

input_csv_person = input_folder + 'PERSON.csv'
input_csv_location = input_folder + 'LOCATION.csv'
input_csv_visit_occurrence = input_folder + 'VISIT_OCCURRENCE.csv'
input_csv_procedure_occurrence = input_folder + 'PROCEDURE_OCCURRENCE.csv'
input_csv_condition_occurrence = input_folder + 'CONDITION_OCCURRENCE.csv'

output_csv_result = output_folder + 'RESULT.csv'

# Main algo called "myAlgo"
def myAlgo(input_csv_person, input_csv_location, input_csv_visit_occurrence, 
            input_csv_procedure_occurrence, input_csv_condition_occurrence):
 
    print("this is my super algo")
    duplicateDF = pd.DataFrame({'duplicate': ["1,2,3", "4,5,6", "7,8,9"]})

    # use to_csv to write the csv
    # separator = ,
    # no index, no header 
    duplicateDF.to_csv(path_or_buf=output_csv_result, sep=',', index=False, header=False)


# hospitals will run your function call 'myAlgo', your fonction have to produce a csv
myAlgo(input_csv_person, input_csv_location, input_csv_visit_occurrence, 
            input_csv_procedure_occurrence, input_csv_condition_occurrence)
